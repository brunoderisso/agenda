var express = require('express');
var app = express();

app.use(express.static('public'));
app.get('/', function (req, res) {
   res.sendFile( __dirname + "/view/" + "index.htm" );
})

app.get('/busca', function (req, res) {
   res.sendFile( __dirname + "/view/busca/" + "index.htm" );
})

app.get('/novo', function (req, res) {
   res.sendFile( __dirname + "/view/novo/" + "index.htm" );
})

app.get('/style', function (req, res) {
   res.sendFile( __dirname + "/css/" + "style.css" );
})

app.use('/image', express.static(__dirname + '/img'));

var server = app.listen(process.env.PORT, function () {
   var host = server.address().address
   var port = server.address().port
   console.log("Example app listening at http://%s:%s", host, port)

})